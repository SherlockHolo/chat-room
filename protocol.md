# Protocol

#### use json format:
```json
{
    "key1": "data1",
    "key2": "data2"
}
```

## client
### handshake
|pid|nick|
|:-:|:-:|
|random data(16 bytes)|string|

### send message
|type|message|
|:-:|:-:|
|(1 byte length)|content|

#### type
|string|picture|video|
|:-:|:-:|:-:|
|01|02|03|

#### content
|string|picture|video|
|:-:|:-:|:-:|
|string|picture ID|video ID|

### recv message
|nick|message|send_time|
|:-:|:-:|:-:|
|string|content|unix time(server receive message time)|

### goodbye
planning
